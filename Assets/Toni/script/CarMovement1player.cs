﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement1player : MonoBehaviour
{
    public bool _terminarCarrera;
    private Collider2D _collider;
    private float _duracionBomba = 0.5f;
    public bool _EscudoOn;
    public bool _BombPU;
    public GameObject _instantiation;
    public bool _onPista;
    public bool _instantiator;
    public bool _salidaOnBarranco;
    public bool _caidaOnBarranco;
    private float _vBarranco = 150;
    [Header("Controles PowerUps")]
    private Regalos _regalos;
    public GameObject _misil;
    public Semaforo _semaforo;
    public float _velocidadSalto;
    public bool _saltoOn;
    private Vector3 _escaladisminuida;
    public bool _caidaOn;
    [SerializeField]
    private Vector3 _escalaInicial;
    [SerializeField]
    private Vector3 _escalaAumentada;
    //ACEITE-----
    [Header("Ajustes Aceite")]
    public Transform _coche;
    //public Rigidbody2D _cocheRB;
    public Animator _AlphaGirar;
    public bool _aceiteOn = false;
    private float _duraciónAceite = 1;
    public float _velocidadAceite = 100;
    private Vector2 _añadirVelocidad;
    //TERMINA ACEITE----
    //TURBO-------
    [Header("Ajustes Turbo")]
    public bool turboOn = false;
    private float _duracionTurbo = 0.5f;
    public float _añadidoTurbo;
    [Header("Todo lo demás")]
    public Camera _Camera;
    private bool _hacerZoom = false;

    public Transform _instanciaMisil;
    public Transform[] _instanciamisiles;

    //declaramos la variable para poder restarle velocidad cuando se salga de la pista
    public float _restarVelocidad;
    //Jugador que controla el car
    public int _playerNumber = 1;

    //Velocidad en unidades por segundo
    public float _speed;

    //Grados que gira por segundo
    public float _turnSpeed = 180f;

    //Sonidos del car
    //public AudioSource _audioSource;
    //public AudioClip _audioIdling;
    //public AudioClip _audioDriving;
    //Variable para establecer el tono del sonido
    public float _pitchRange = 0.2f;

    //Variables para guardar los sprites
    public SpriteRenderer _spriteRenderer;
    public Sprite[] _arraySprites;
    //Puntero para guardar la posición del sprite actual en el array
    private int _spriteActual;

    //Variables que guardan el nombre de los ejes correspondiente a este jugador
    private string _movementAxisName;
    private string _turnAxisName;

    //rigidbody del car para poder moverlo
    private Rigidbody2D _rigidbody2d;
    //transform del car
    private Transform _transform;

    //Variables para guardar los valores de los axis
    private float _movementInputValue;
    private float _turnInputValue;

    private float _movementInputValueForSprite;
    private float _turnInputValueForSprite;

    //Variable para guardar el tono inicial del audioSource
    private float _inicialPitch;

    [Header("Ajustes velocidad")]
    [Tooltip("Normalmente la resta el resultado de moveForce - 700")]
    public float _restaTriggerPista = 450;
    public float _velocidadNormal;
    //Variables para aplicar inercia
    public float moveForce = 2.0f;
    public float maxSpeed = 4.5f;
    private Vector3 v;
    public GameObject objetoMisil;
    public bool _misilOn;

    //Método que se ejecuta cuando el objeto se activa en escena
    private void OnEnable()
    {
        //El rigidbody es afectado por las físicas
        _rigidbody2d.isKinematic = false;

        //Ponemos los valores de movimiento a cero
        _movementInputValue = 0f;
        _turnInputValue = 0f;

        // Ponemos los valores del movimiento de los sprites a cero
        _movementInputValueForSprite = 0f;
        _turnInputValueForSprite = 0f;
    }
    //Método que se ejecuta cuando el objeto se desactiva
    private void OnDisable()
    {
        //El rigidbody deja de ser afectado por las físicas
        _rigidbody2d.isKinematic = true;
    }
    private void Awake()
    {
        //Referencia al rigidbody
        _rigidbody2d = GetComponent<Rigidbody2D>();

        //Referencia al spriteRenderer
        _spriteRenderer = GetComponent<SpriteRenderer>();

        //Referencia al transform
        _transform = GetComponent<Transform>();
    }
    // Use this for initialization
    void Start()
    {
        _regalos = GetComponent<Regalos>();
        _collider = GetComponent<Collider2D>();

        //Inicialización de las variables que contienen los nombres de los axes
        _escalaInicial = transform.localScale;
        _escalaAumentada = _escalaInicial * 3;
        _escaladisminuida = _escalaInicial / 1000;

        _movementAxisName = "Vertical" + _playerNumber;
        _turnAxisName = "Horizontal" + _playerNumber;

        //Guardamos el original Pitch
        // _inicialPitch = _audioSource.pitch;
        ;
        _instantiation = GameObject.Find("Instancia Coche");



        //Ponemos el sprite correspondiente
        SetSprite();
    }
    // Update is called once per frame
    void Update()
    {
        if (_misilOn == true)
        {
            StartCoroutine(QuitadorDeMisilDeEmergencia(1f));
        }
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        // MisilInstanciado();
        //}


        //Debug.Log("La duración del turbo es: " + _duracionTurbo);
        //guardar la entrada de teclado
        _movementInputValue = Input.GetAxis(_movementAxisName);
        _turnInputValue = Input.GetAxis(_turnAxisName);

        // Cogemos el AxisRaw para cambiar los sprites más rápidos -> LMR
        _movementInputValueForSprite = Input.GetAxisRaw(_movementAxisName);
        _turnInputValueForSprite = Input.GetAxisRaw(_turnAxisName);

        if (_aceiteOn == false)
        {
            //vector para aplicar la inercia
            v = new Vector3(Input.GetAxis(_turnAxisName), Input.GetAxis(_movementAxisName), 0.0f);
        }


        if (_hacerZoom == true)
        {
            _Camera.orthographicSize = Mathf.Lerp(_Camera.orthographicSize, 65f, Time.deltaTime);
            //_Camera.orthographicSize = Mathf.Lerp(30f, 50f, 40f);
        }
        else
        {
            _Camera.orthographicSize = Mathf.Lerp(_Camera.orthographicSize, 45f, Time.deltaTime);
        }

        if (_saltoOn == true)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, _escalaAumentada, Time.deltaTime * 3);
            _rigidbody2d.velocity = v.normalized * _velocidadSalto;
        }
        if (_saltoOn == false)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, _escalaInicial, Time.deltaTime * 7);
        }
        if (_caidaOn == true)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, _escaladisminuida, Time.deltaTime * 6);
        }
        if (_caidaOn == false)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, _escalaInicial, Time.deltaTime * 15);
            _instantiator = false;
        }

        if (_caidaOnBarranco == true)
        {
            caidaBarranco();
        }
        if (_salidaOnBarranco == true)
        {
            Invoke("CocheCaer", 0.01f);
        }

        if (_instantiator == true)
        {
            Invoke("Reaparecer", 0.5f);
        }


        EngineAudio();

        /*  if (Input.GetKeyDown(KeyCode.Space))
          {
              GameObject objetivoMisil = Instantiate(_misil);
              objetoMisil.gameObject.transform.parent = transform;
              objetoMisil.gameObject.transform.position = Vector3.zero;
              //TODO
              //objetoMisil.IniciarMisil(gameObject.GetComponent<ControladorCheckPoint>()._actualCheckPoint); 
          }*/
    }
    //Método que gestiona el audioclip que tiene que sonar en cada momento
    //dependiendo de si el coche está parado o no
    private void EngineAudio()
    {
        //Sabemos si el coche se está moviendo por el valor de las variables que guardan
        //la entrada de teclado: _movementInputValue y _turnInputValue
        //Cuando el valor de ambas sea 0 o muy cerca de 0 el motor está parado
        //Cuando alguna de ellas supere un valor mínimo el motor se está moviendo
        if (Mathf.Abs(_movementInputValue) < 0.1f && Mathf.Abs(_turnInputValue) < 0.1f)
        {
            //El motor está parado
            //Comprobamos si está sonando el motor en movimiento
            //  if (_audioSource.clip == _audioDriving)
            {
                //Cambiamos el sonido
                //  _audioSource.clip = _audioIdling;
                //Establecemos un tono aleatorio
                // _audioSource.pitch = Random.Range(_inicialPitch - _pitchRange, _inicialPitch + _pitchRange);
                // _audioSource.Play();
            }
        }
        else
        {
            //El motor está en marcha
            //Comprobamos si está sonando el motor parado
            // if (_audioSource.clip == _audioIdling)
            {
                //Cambiamos el sonido
                // _audioSource.clip = _audioDriving;
                //Establecemos un tono aleatorio
                // _audioSource.pitch = Random.Range(_inicialPitch - _pitchRange, _inicialPitch + _pitchRange);
                // _audioSource.Play();
            }
        }
    }
    //Este método se ejecuta en cada paso del motor de físicas
    private void FixedUpdate()
    {
        if (_EscudoOn == true)
        {
            _aceiteOn = false;
            _BombPU = false;
            _misilOn = false;
        }


        if (_aceiteOn == false && _semaforo._controlsOn == true && _BombPU == false && _caidaOn == false && _misilOn == false && _terminarCarrera == false)
        {
            //Aplicamos el movimiento y el giro
            Move();

            //Ponemos el sprite correspondiente
            SetSprite();

            NoGirarCoche();
            if (_duraciónAceite <= 0)
            {
                _duraciónAceite = 1;
            }
            if (_duracionBomba <= 0)
            {
                _duracionBomba = 0.5f;
            }


            _AlphaGirar.enabled = false;
        }
        else
        {
            if (_BombPU == true)
            {
                _AlphaGirar.enabled = true;
                _duracionBomba -= Time.deltaTime;
            }
            if (_aceiteOn == true)
            {
                _rigidbody2d.velocity = v.normalized * _velocidadAceite;
                _duraciónAceite -= Time.deltaTime;
                _AlphaGirar.enabled = true;
            }
            if (_duraciónAceite <= 0)
            {
                moveForce = _velocidadNormal;
                _aceiteOn = false;

            }
            if (_duracionBomba <= 0)
            {
                _BombPU = false;
            }
        }
        if (turboOn == true)
        {
            _duracionTurbo -= Time.deltaTime;
            OnTurboOn();
        }
        if (_duracionTurbo <= 0)
        {
            turboOn = false;
            OnTurboOff();
        }
    }
    //Método que gestiona el movimiento
    public void Move()
    {


        //Vector que guarda cuanto se va a mover el car
        Vector2 movement = transform.up * _movementInputValue * _speed * Time.deltaTime;

        //Calculamos cuanto va a girar el car
        Vector2 turn = transform.right * _turnInputValue * _turnSpeed * Time.deltaTime;

        //Aplicamos el movimiento
        _rigidbody2d.MovePosition(_rigidbody2d.position + movement + turn.normalized);
        _rigidbody2d.AddForce(v.normalized * moveForce);
        _rigidbody2d.velocity = Vector3.ClampMagnitude(_rigidbody2d.velocity, maxSpeed);
    }
    private void SetSprite()
    {
        // Cambiado el método para adaptarlo a las variables de cambio de Sprite -> LMR

        Sprite spriteNuevo = _spriteRenderer.sprite;
        //Comprobamos el movimiento del coche
        if (_movementInputValueForSprite != 0 || _turnInputValueForSprite != 0)
        {
            //el coche se mueve
            //¿Hacia dónde?

            if (_movementInputValueForSprite > 0)
            {
                if (_turnInputValueForSprite > 0)
                {
                    spriteNuevo = _arraySprites[7];
                }
                else if (_turnInputValueForSprite < 0)
                {
                    spriteNuevo = _arraySprites[1];
                }
                else
                {
                    spriteNuevo = _arraySprites[0];
                }
            }
            else if (_movementInputValueForSprite < 0)
            {
                if (_turnInputValueForSprite > 0)
                {
                    spriteNuevo = _arraySprites[5];
                }
                else if (_turnInputValueForSprite < 0)
                {
                    spriteNuevo = _arraySprites[3];
                }
                else
                {
                    spriteNuevo = _arraySprites[4];
                }
            }
            else //eje vertical = 0
            {
                if (_turnInputValueForSprite > 0)
                {
                    spriteNuevo = _arraySprites[6];
                }
                else if (_turnInputValueForSprite < 0)
                {
                    spriteNuevo = _arraySprites[2];
                }
            }
        }

        //Establecemos el nuevo sprite comprobando primero que no sea el actual
        if (_spriteRenderer.sprite != spriteNuevo)
        {
            _spriteRenderer.sprite = spriteNuevo;
        }

    }
    //hacemos que al pisar la pista reste velocidad a los coches
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "TriggerPista" && _regalos._shieldOn == false)
        {
            moveForce = _restaTriggerPista;
            _onPista = true;
        }
        if (collision.gameObject.tag == "TriggerCamara")
        {
            _hacerZoom = true;
        }
        if (collision.gameObject.tag == "TriggerAceite")
        {
            _aceiteOn = true;
        }
        if (collision.gameObject.tag == "turbo")
        {
            turboOn = true;
        }
        if (collision.gameObject.tag == "TriggerBarranco")
        {
            _caidaOnBarranco = true;
            _caidaOn = true;
        }

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "TriggerPista")
        {
            moveForce = _velocidadNormal;
            _onPista = false;
        }
        if (collision.gameObject.tag == "TriggerCamara")
        {
            _hacerZoom = false;
        }
        if (collision.gameObject.tag == "TriggerBarranco")
        {
            //_caidaOn = true;
            //_semaforo._controlsOn = false;
            _caidaOnBarranco = false;
            _instantiator = true;
            _salidaOnBarranco = true;
        }

    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (_aceiteOn == false)
        {

            if (collision.gameObject.tag == "TriggerPista")
            {
                if (turboOn == true && _onPista == true)
                {
                    moveForce = 2050;
                }
                else if (_regalos._shieldOn == false)
                {
                    moveForce = 450;
                }
            }
        }
        if (collision.gameObject.tag == "TriggerBarranco")
        {
            //_caidaOn = false;
            _caidaOnBarranco = true;
        }
    }
    private void GirarCoche()
    {
        _AlphaGirar.enabled = true;

    }
    private void NoGirarCoche()
    {
        _AlphaGirar.enabled = false;
    }
    private void OnTurboOn()
    {
        if (turboOn == true)
        {
            moveForce = 2500;
        }
    }
    private void OnTurboOff()
    {
        if (turboOn == false)
        {
            moveForce = 1150;
            _duracionTurbo = 0.5f;
        }
    }
    public void CocheCaer()
    {
        _spriteRenderer.enabled = false;
        _rigidbody2d.velocity = Vector2.zero;
        _rigidbody2d.gravityScale = 0;
    }
    public void caidaBarranco()
    {
        //_semaforo._controlsOn = false;
        _rigidbody2d.gravityScale = 100;
        _rigidbody2d.velocity = v.normalized * _vBarranco;
    }
    public void Reaparecer()
    {
        transform.position = _instantiation.transform.position;
        _caidaOn = false;
        _spriteRenderer.enabled = true;
        _semaforo._controlsOn = true;
        _salidaOnBarranco = false;
    }
    public void MisilInstanciado()
    {
        Misil objetoMisil = Instantiate(_misil, _instanciaMisil.position, _instanciaMisil.rotation).GetComponent<Misil>();
        PathFollowingController2D pathFollowing = objetoMisil.GetComponent<PathFollowingController2D>();
        pathFollowing._pathRoot = GameObject.Find("Path Misil").transform;
        //objetoMisil.IniciarMisil(gameObject.GetComponent<ControladorCheckPoint>()._actualCheckPoint);
    }
    private IEnumerator QuitadorDeMisilDeEmergencia(float tem)
    {
        yield return new WaitForSeconds(0.5f);
        _misilOn = false;
        _AlphaGirar.enabled = false;
    }
}