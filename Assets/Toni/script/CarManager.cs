﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
//clase que guarda información sobre cada eje del coche
public class EjeInfo
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public bool motor;
    public bool control;
    
}


public class CarManager : MonoBehaviour {

    //Lista para guardar los ejes, trasero y delantero
    public List<EjeInfo> _ejesInfo;
    //Máxima rotación que el motor puede aplicar a la rueda
    public float _maxMotorTorque;
    //Máximo giro de la rueda
    public float _maxSteeringAngle;


    //Método que encuentra los sprits de la rueda y le aplica
    //correctamente el transform del collider
    public void ApplyLocalPositionToVisuals(WheelCollider collider)
    {
        //Comprueba si el collider tiene hijos, si no tiene termina la función
        if(collider.transform.childCount == 0)
        {
            return;
        }

        //recojemos en visualWheel el collider del hijo (el objeto que representa la rueda)
        Transform visualWheel = collider.transform.GetChild(0);

        //guardamos en las variables position y rotation la posición y rotación del collider padre
        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);

        //girar las ruedas
        rotation = new Quaternion(rotation.x, rotation.y, rotation.z +90f, rotation.w +90f);

        //le aplicamos la position y rotation al collider hijo
        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }

    public void FixedUpdate()
    {
        
        float motor = _maxMotorTorque * Input.GetAxis("Vertical");
        float giro = _maxSteeringAngle * Input.GetAxis("Horizontal");
       

        for (int i = 0; i < _ejesInfo.Count; i++)
        {
            if (_ejesInfo[i].control)
            {
                _ejesInfo[i].leftWheel.steerAngle = giro;
                _ejesInfo[i].rightWheel.steerAngle = giro;
            }
            if (_ejesInfo[i].motor)
            {
                _ejesInfo[i].leftWheel.motorTorque = motor;
                _ejesInfo[i].rightWheel.motorTorque = motor;
            }
            //Llamamos a ApplyLocalPositionToVisuals para colocar los colliders de las ruedas
            ApplyLocalPositionToVisuals(_ejesInfo[i].leftWheel);
            ApplyLocalPositionToVisuals(_ejesInfo[i].rightWheel);
            
        }


    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
	}
}
