﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GiroCar : MonoBehaviour {

    //Transform del objeto referencia
    public Transform _transform;

    //Transform del sprite
    private Transform _miTransform;

    //Texto para mostrar el giro
    public Text _txtGiro;

    //Variable para guardar la rotación en grados del objeto
    private float _rotacionY;

    //Variables para guardar los sprites
    public SpriteRenderer _spriteRenderer;
    public Sprite[] _arraySprites;
    //Puntero para guardar la posición del sprite actual en el array
    private int _spriteActual;


    // Use this for initialization
    void Start () {

        //Referencia al transform
        _miTransform = GetComponent<Transform>();

        //Referencia al spriteRenderer
        _spriteRenderer = GetComponent<SpriteRenderer>();

        //Inicializa la rotación 
        _rotacionY = 0;
    }
	
	// Update is called once per frame
	void Update () {

        _rotacionY = _transform.eulerAngles.y;

        if(_rotacionY < 0)
        {
            _txtGiro.text = "Giro izquierda " + _rotacionY;
        }else if(_rotacionY > 0)
        {
            _txtGiro.text = "Giro derecha " + _rotacionY;
        }else
        {
            _txtGiro.text = "Sin giro " + _rotacionY;
        }
        _miTransform.eulerAngles = new Vector3 (_miTransform.eulerAngles.x, 0f, _miTransform.eulerAngles.z);
        SetSprite();
	}

    private void SetSprite()
    {
        Sprite spriteNuevo = _spriteRenderer.sprite;

        //Comprobamos la rotacion del objeto y le aplicamos el sprite correspondiente
        if(_rotacionY < 22.5f || _rotacionY > 382.5f)
        {
            spriteNuevo = _arraySprites[0];
        }else if(_rotacionY > 22.5f && _rotacionY < 67.5f)
        {
            spriteNuevo = _arraySprites[1];
        }else if(_rotacionY > 67.5f && _rotacionY < 112.5f)
        {
            spriteNuevo = _arraySprites[2];
        }
        else if (_rotacionY > 112.5f && _rotacionY < 157.5f)
        {
            spriteNuevo = _arraySprites[3];
        }
        else if (_rotacionY > 157.5f && _rotacionY < 202.5f)
        {
            spriteNuevo = _arraySprites[4];
        }
        else if (_rotacionY > 202.5f && _rotacionY < 247.5f)
        {
            spriteNuevo = _arraySprites[5];
        }
        else if (_rotacionY > 247.5f && _rotacionY < 337.5f)
        {
            spriteNuevo = _arraySprites[6];
        }
        else if (_rotacionY > 337.5f && _rotacionY < 382.5f)
        {
            spriteNuevo = _arraySprites[7];
        }


        //Establecemos el nuevo sprite comprobando primero que no sea el actual
        if (_spriteRenderer.sprite != spriteNuevo)
        {
            _spriteRenderer.sprite = spriteNuevo;
        }

    }
}
