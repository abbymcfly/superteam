﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;


public class PlayerPositionController : MonoBehaviour {

    public List<ControladorCheckPoint> _corredores;
    public List<Transform> _checkPoints;
    //prueba para poner la posicion en el canvas
    public Text _posicionText;
   



    public void Start()
    {
        // ORDENAMOS LOS CHECKPOINTS DE MENOR A MAYOR
        _checkPoints = _checkPoints.OrderBy(checkpointTransform => int.Parse(checkpointTransform.name)).ToList();
    }

    // Update is called once per frame
     void Update ()
    {
        
        // ORDENAMOS LOS CORREDORES PRIMERO POR LA VUELTA LUEGO POR CHECKPOINT Y LUEGO POR DISTANCIA AL PRÓXIMO CHECKPOINT
        _corredores = _corredores.OrderByDescending(corredor => corredor._lapNumber).ThenByDescending(corredor => corredor._actualCheckPoint).ThenBy(corredor => Vector3.Distance(corredor.transform.position, _checkPoints[corredor._actualCheckPoint == _checkPoints.Count ? 0 : corredor._actualCheckPoint].position)).ToList();
        int posicion = 1;
        
        foreach (ControladorCheckPoint ccp in _corredores)
        {//intento que aparezca la posicion en el canvas 
            //_posicionText.text = posicion.ToString();// esto saca un numero en el canvas pero no se actualiza con la posicion 
            //intento que aparezca la posicion en el canvas 
            //_posicionText.text = posicion.ToString();// esto saca un numero en el canvas pero no se actualiza con la posicion 
            //Debug.Log(ccp.name + " está en la posición " + posicion);        
            posicion += 1;

            ccp._position = posicion;
            posicion += 1;    
        }
    }

    public string GetPosition(ControladorCheckPoint player)
    {
        return (_corredores.IndexOf(player) + 1).ToString();
    }
    public GameObject objetivoMisil(int checkpoint)
    {
        for (int i = 0; i < _checkPoints.Count; i++)
        {
            checkpoint++;
            if (checkpoint == _checkPoints.Count)
            {
                checkpoint = 0;
            }
            foreach (ControladorCheckPoint ccp in _corredores)
            {
                if (ccp._actualCheckPoint == checkpoint)
                {
                    return ccp.gameObject;
                }
            }
        }
        return null;
    }
}
