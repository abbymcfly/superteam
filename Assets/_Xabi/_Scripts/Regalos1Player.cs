﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Regalos1player : MonoBehaviour
{

    private Collider2D _lanzadorMisil;
    public bool _RegaloOn = false;
    public int _resultado;
    public CarMovement _car;
    private float _contadorBoost = 1f;
    public GameObject _Misil;
    public GameObject _cajaBomba;
    public GameObject _Escudo;
    public GameObject _boost;
    public Image _MisilI;
    public Image _cajaBombaI;
    public Image _EscudoI;
    public Image _boostI;

    public Image _MisilI2;
    public Image _cajaBombaI2;
    public Image _EscudoI2;
    public Image _boostI2;
    public Transform _instanciaCajaBomba;
    public GameObject _prefabCajaBomba;
    private bool _cajaBombaOn;
    private CarMovement _cm;
    public SpriteRenderer _Shield;
    [HideInInspector]
    public float _contadorEscudo = 2.5f;
    public bool _shieldOn;
    public Transform _instanciaMisil;
    public GameObject _prefabMisil;
    public bool _misilOn;
    private string _botonDeDisparo;
    public int _numeroDeJugador;
    public bool _OnePlayer;

    private void Start()
    {

        _MisilI = GameObject.Find("Pantalla Regalos 1 Misil").GetComponent<Image>();
        _EscudoI = GameObject.Find("Pantalla Regalos 1 Escudo").GetComponent<Image>();
        _cajaBombaI = GameObject.Find("Pantalla Regalos 1 Caja Bomba").GetComponent<Image>();
        _boostI = GameObject.Find("Pantalla Regalos 1 Boost").GetComponent<Image>();

        _botonDeDisparo = "Fire" + _numeroDeJugador;
        _lanzadorMisil = GetComponent<Collider2D>();
        _cm = GetComponent<CarMovement>();
        /*if (_numeroDeJugador == 1)
        {
            _Misil = GameObject.Find("Pantalla Regalos 1 Misil");
            _Escudo = GameObject.Find("Pantalla Regalos 1 Escudo");
            _cajaBomba = GameObject.Find("Pantalla Regalos 1 Caja Bomba");
            _boost = GameObject.Find("Pantalla Regalos 1 Boost");
        }
        if (_numeroDeJugador == 2)
        {
            _Misil = GameObject.Find("Pantalla Regalos 2 Misil");
            _Escudo = GameObject.Find("Pantalla Regalos 2 Escudo");
            _cajaBomba = GameObject.Find("Pantalla Regalos 2 Caja Bomba");
            _boost = GameObject.Find("Pantalla Regalos 2 Boost");
        }*/

    }
    void Update()
    {
        if (_car._caidaOn == true)
        {
            _contadorEscudo = 0;
        }
        if (_shieldOn == true)
        {
            _contadorEscudo -= Time.deltaTime;

            if (_contadorEscudo <= 0)
            {
                _Shield.enabled = false;
                _RegaloOn = false;
                _cm._EscudoOn = false;
                //_Escudo.SetActive(false);
                _shieldOn = false;
                StartCoroutine(RecargarEscudo(0.1f));
            }
        }
        if (_misilOn == true)
        {
            QuitarMisil();
        }

        //Debug.Log("El power up es "+ _resultado);
        if (_cajaBombaOn == true)
        {
            QuitarCaja();
        }

        if (_contadorBoost <= 0)
        {
            _contadorBoost = 1f;
        }
        //Debug.Log("Queda esto de power up: " + _contadorBoost);
        ObtenerRegalo();
    }


    public void CalcularRegalo()
    {
        _resultado = Random.Range(1, 5);
    }

    private void ObtenerRegalo()
    {
        //Boost / Turbo
        if (_RegaloOn == true)
        {
            if (_resultado == 1)
            {
                if (_numeroDeJugador == 1)
                {
                    _boostI.enabled = true;
                }
                


                if (_RegaloOn == true && Input.GetButton(_botonDeDisparo))
                {
                    _contadorBoost -= Time.deltaTime;
                    _car.turboOn = true;
                }
                if (_contadorBoost <= 0)
                {
                    _car.turboOn = false;
                    if (_numeroDeJugador == 1)
                    {
                        _boostI.enabled = false;
                    }
                   

                    if (_car.turboOn == false)
                    {
                        _RegaloOn = false;
                    }
                }
            }
            // Misil
            else if (_resultado == 2)
            {
                if (_numeroDeJugador == 1)
                {
                    _MisilI.enabled = true;
                }
                
                if (_RegaloOn == true && Input.GetButtonDown(_botonDeDisparo))
                {
                    Misil misilSc = Instantiate(_prefabMisil, _instanciaMisil.position, _instanciaMisil.rotation).GetComponent<Misil>();
                    misilSc._parentName = this.name;
                    PathFollowingController2D misilPathController = misilSc.GetComponent<PathFollowingController2D>();
                    misilPathController._pathRoot = GameObject.Find("Path Misil").transform;

                    _misilOn = true;
                    _MisilI.enabled = false;
                    //Physics2D.IgnoreCollision(colliderMisil, _lanzadorMisil);
                }
            }
            //Caja Bomba
            else if (_resultado == 3)
            {
                if (_numeroDeJugador == 1)
                {
                    _cajaBombaI.enabled = true;
                }
                if (_numeroDeJugador == 2)
                {
                    _cajaBombaI2.enabled = true;
                }
                if (_RegaloOn == true && Input.GetButtonDown(_botonDeDisparo))
                {

                    RegaloBomba _cB = Instantiate(_prefabCajaBomba, _instanciaCajaBomba.position, _instanciaCajaBomba.rotation).GetComponent<RegaloBomba>();
                    _cajaBombaOn = true;

                    if (_numeroDeJugador == 1)
                    {
                        _cajaBombaI.enabled = false;
                    }
                }

                /*if (_contadorBoost <= 0)
                {
                    _cajaBomba.SetActive(false);
                    _car.turboOn = false;

                    if (_car.turboOn == false)
                    {
                        _RegaloOn = false;
                    }
                }*/
            }
            //Escudo
            else if (_resultado == 4)
            {
                //Debug.Log("Escudo");
                if (_numeroDeJugador == 1)
                {
                    _EscudoI.enabled = true;
                }
               

                if (_RegaloOn == true && Input.GetButtonDown(_botonDeDisparo))
                {

                    //Debug.Log("Enter");
                    _cm._EscudoOn = true;
                    _shieldOn = true;
                    _Shield.enabled = true;
                    if (_numeroDeJugador == 1)
                    {
                        _EscudoI.enabled = false;
                    }
                   
                    _RegaloOn = false;
                }

                /*if (_contadorBoost <= 0)
                {
                    _Escudo.SetActive(false);
                    _car.turboOn = false;

                    if (_car.turboOn == false)
                    {
                        _RegaloOn = false;
                    }
                }*/
            }
        }
    }
    private void QuitarCaja()
    {
        _cajaBombaOn = false;
        _RegaloOn = false;
    }
    private void QuitarMisil()
    {
        _misilOn = false;
        _RegaloOn = false;
    }
    private IEnumerator RecargarEscudo(float tem)
    {
        yield return new WaitForSeconds(0.5f);
        _contadorEscudo = 2.5f;
    }

}
