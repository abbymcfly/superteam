﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetFollowJ2 : MonoBehaviour {
    public Transform target;
    public Vector3 offset  = new Vector3(0f, 10f, -16f);
    public GameObject _coche1;

    private CarMovement _cm;

    public Transform _coche1T;


    // Use this for initialization
    void Start() {
        _coche1 = GameObject.Find("J2");
        _cm = _coche1.GetComponent<CarMovement>();
        _coche1T = _coche1.transform;
        //offset = target.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (_cm._playerNumber == 2)
        {
            target = _coche1T;
        }
    }
    private void LateUpdate()
    {
        transform.position = target.position + offset;

    }
}
