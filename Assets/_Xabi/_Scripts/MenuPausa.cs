﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuPausa : MonoBehaviour
{
    public Text _pausaText;
    public GameObject _panel;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            _panel.SetActive(true);  
            if (Time.timeScale == 1.0F)
            {
                Time.timeScale = 0F;
                _pausaText.text = "Pausa ";

            }
            else
            {
                _panel.SetActive(false);
                Time.timeScale = 1.0F;
            }                           
        }
    }
    public void SalirDePausa()
    {
        _panel.SetActive(false);
        Time.timeScale = 1.0f;
    }
    public void SalirDelJuego()
    {
        Application.Quit();
    }
    public void VolverAlMenu()
    {
        SceneManager.LoadScene(0);
    }

public void RecargarEscena()
    {
        SceneManager.LoadScene(2);
    }
}





