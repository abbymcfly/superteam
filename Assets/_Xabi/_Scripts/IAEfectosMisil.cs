﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAEfectosMisil : MonoBehaviour {
    public IAEfectosCarrera _IA;
    private UnitySteer2D.Behaviors.AutonomousVehicle2D _AV2DIA;
    public bool _misilOn;

    // Use this for initialization
    void Start ()
    {
        _IA = GetComponentInChildren<IAEfectosCarrera>();
        _AV2DIA = GetComponent<UnitySteer2D.Behaviors.AutonomousVehicle2D>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	
        if (_IA._shieldOn == true)
        {
            _misilOn = false;
        }	
        if (_misilOn == true)
        {
            _AV2DIA._misilOn = true;
            _AV2DIA.MaxForce = 0;
            _AV2DIA.MaxSpeed = 0;
            StartCoroutine("QuitarBombPUIA");
        }
	}
    private IEnumerator QuitarBombPUIA()
    {
        yield return new WaitForSeconds(2f);
        _misilOn = false;
        _AV2DIA.MaxForce = 81;
        _AV2DIA.MaxSpeed = 1000;
        _AV2DIA._misilOn = false;
    }
}
