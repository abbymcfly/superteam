﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IATerminarCarrera : MonoBehaviour {
    public ControladorCheckPoint _ccp;
    public UnitySteer2D.Behaviors.AutonomousVehicle2D _AV2DIA;

    private void Start()
    {
        _ccp = GetComponent<ControladorCheckPoint>();
        _AV2DIA = GetComponent<UnitySteer2D.Behaviors.AutonomousVehicle2D>();
    }
    void Update ()
    {
		if (_ccp._lapNumber == 5)
        {
            _AV2DIA._terminarPartida = true;
        }
	}
}
