﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitarEscudoTrasAtaque : MonoBehaviour {
    private Regalos _regalos;
    private void Start()
    {
        _regalos = GetComponentInParent<Regalos>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "RegaloBomba")
        {
            StartCoroutine(CajaBomba(0.2f));
        }
    }
    private IEnumerator CajaBomba (float tem)
    {
        yield return new WaitForSeconds(0.2f);
        _regalos._contadorEscudo = 0;
    }
}
