﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscudoIA : MonoBehaviour {

    public bool _triggerOff;
    private IAEfectosCarrera _IA;
    private Collider2D _collider;
    private RegaloBomba _bomba;

    // Use this for initialization
    void Start()
    {
        _IA = GetComponentInChildren<IAEfectosCarrera>();
        _collider = GetComponentInChildren<Collider2D>();
    }
    private void Update()
    {
        if (_triggerOff == true)
        {
            StartCoroutine("DejarDeSerTriggerIA");
        }
        if (_triggerOff == true)
        {
            StartCoroutine("ResetearTriggerIA");
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "RegaloBomba")
        {
            if (_IA._shieldOn == true)
            {
                _collider.isTrigger = true;
                _triggerOff = true;
            }
        }
    }
    private IEnumerator DejarDeSerTriggerIA()
    {
        yield return new WaitForSeconds(1f);
        _collider.isTrigger = false;
    }
    private IEnumerator ResetearTriggerIA()
    {
        yield return new WaitForSeconds(2f);
        _triggerOff = false;
    }

}
