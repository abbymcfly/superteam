﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAEfectosCarrera : MonoBehaviour
{
    //private Rigidbody2D _rigidbody2d;
    //public Semaforo _semaforo;
    public bool _aceiteOn;
    public Animator _animator;
    [HideInInspector]
    public float _tiempoAceite = 1f;
    //[HideInInspector]
    public bool _caidaOn;
    public bool _instantiator;
    public bool _salidaOnBarranco;
    public bool _caidaOnBarranco;
    private float _vBarranco = 150;
    private SpriteRenderer _spriteRenderer;
    public GameObject _instantiation;
    public Vector3 _escaladisminuida;
    public Vector3 _escalaInicial;

    //------------ REGALOS / POWER UPS -------------------
    public bool _shieldOn;
    public SpriteRenderer _Shield;
    [SerializeField]
    private bool _powerUpOn;
    public bool _RegaloOn = false;
    public int _resultado;
    public bool _turboOn;
    private float _contadorBoost = 1f;
    private float _tiempoUsarPU;
    public UnitySteer2D.Behaviors.AutonomousVehicle2D _IAmovimiento;
    public Transform _instanciaCajaBomba;
    public GameObject _prefabCajaBomba;
    private bool _cajaBombaOn;
    private Collider2D _collider;
    private Rigidbody2D _rigidbody2d;
    public Transform _instanciaMisil;
    public GameObject _prefabMisil;
    public bool _misilOn;

    private void Start()
    {
        _animator = GetComponentInParent<Animator>();
        _instantiation = GameObject.Find("Instancia Coche");
        _prefabMisil = (GameObject)Resources.Load("Misil", typeof(GameObject)) as GameObject;
        _prefabCajaBomba = (GameObject)Resources.Load("RegaloBomba", typeof(GameObject)) as GameObject;
        _Shield = GetComponentInParent<SpriteRenderer>();
        _IAmovimiento = GetComponentInParent<UnitySteer2D.Behaviors.AutonomousVehicle2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _rigidbody2d = GetComponentInParent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
    }
    void Update()
    {
        ObtenerRegalo();
        EfectosCarrera();
        Caer();
        //Debug.Log(_resultado);
        //Debug.Log("Queda " + _contadorBoost + " De boost");

        //----------------------- REGALOS Y POWER UPS ----------------------

        //Debug.Log("Queda esto de power up: " + _tiempoUsarPU);
        if (_cajaBombaOn == true)
        {
            _powerUpOn = false;
            _RegaloOn = false;
            Invoke("QuitarCaja", 0.2f);
        }
        if (_misilOn == true)
        {
            _powerUpOn = false;
            _RegaloOn = false;
            Invoke("QuitarMisil", 0.2f);
        }
        if (_shieldOn == true)
        {
            _aceiteOn = false;
            StartCoroutine(TerminarEscudo(3f));
        }
        if (_turboOn == true)
        {
            _IAmovimiento.MaxForce = 250f;
            StartCoroutine(BoostPistaOn(1f));
        }


    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "TriggerAceite")
        {
            _aceiteOn = true;
        }
        if (collision.gameObject.tag == "Regalo")
        {
            CalcularRegalo();
        }
        if (collision.gameObject.tag == "turbo")
        {
            _turboOn = true;
        }
        if (collision.gameObject.tag == "TriggerBarranco")
        {
            _caidaOnBarranco = true;
            _caidaOn = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "TriggerBarranco")
        {
            //_caidaOn = true;
            //_semaforo._controlsOn = false;
            _caidaOnBarranco = false;
            _instantiator = true;
            _salidaOnBarranco = true;
        }
    }
    public void CalcularRegalo()
    {
        _resultado = Random.Range(1, 4);
        if (_resultado == 1)
        {
            StartCoroutine(DoTurbo(3f));
        }

    }
    public void ObtenerRegalo()
    {
        // BOOST / TURBO -------------------
        if (_RegaloOn == true)
        {
            if (_resultado == 1)
            {
                if (_RegaloOn == true && _powerUpOn)
                {
                    _IAmovimiento.MaxForce += 50f;
                    _RegaloOn = false;
                }
            }


            // MISIL --------------------
            else if (_resultado == 2)
            {
                if (_RegaloOn == true && _powerUpOn)
                {
                    Misil misilSc = Instantiate(_prefabMisil, _instanciaMisil.position, _instanciaMisil.rotation).GetComponent<Misil>();
                    misilSc._parentName = this.name;
                    PathFollowingController2D misilPathController = misilSc.GetComponent<PathFollowingController2D>();
                    misilPathController._pathRoot = GameObject.Find("Path Misil").transform;
                    _misilOn = true;
                }
            }
            //CAJA BOMBA ------------------
            else if (_resultado == 3)
            {
                Invoke("DoCajaBomba", 1f);
                //Debug.Log("Boom?");

                if (_RegaloOn == true && _powerUpOn == true)
                {
                    //Debug.Log("BOOM");
                    RegaloBomba _cB = Instantiate(_prefabCajaBomba, _instanciaCajaBomba.position, _instanciaCajaBomba.rotation).GetComponent<RegaloBomba>();
                    _cajaBombaOn = true;
                }

            }
            //ESCUDO ---------------------------------  
            else if (_resultado == 4)
            {
                StartCoroutine(DoShield(0.5f));
                if (_RegaloOn == true && _powerUpOn)
                {
                    _Shield.enabled = true;
                    _shieldOn = true;
                }
            }
            }
        }
    public void EfectosCarrera()
    {
        if (_aceiteOn == true)
        {
            _animator.enabled = true;
            _tiempoAceite -= Time.deltaTime;
        }
        if (_tiempoAceite <= 0)
        {
            _aceiteOn = false;
            Invoke("RestablecerAceite", 0.2f);
            if (_aceiteOn == false)
            {
                _animator.enabled = false;
            }
        }
        if (_tiempoAceite <= 0)
        {
            _tiempoAceite = 1f;
        }
       

    }
    public void Caer()
    {
        if (_caidaOn == true)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, _escaladisminuida, Time.deltaTime * 6);
            //_IAmovimiento.MaxForce -= 81;
            //_IAmovimiento.MaxSpeed = 1000;
        }
        if (_caidaOn == false)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, _escalaInicial, Time.deltaTime * 15);
            _instantiator = false;
            //_IAmovimiento.MaxForce = 81;
            //_IAmovimiento.MaxSpeed = 1000;
        }

        if (_caidaOnBarranco == true)
        {
            caidaBarranco();
        }
        if (_salidaOnBarranco == true)
        {
            Invoke("CocheCaer", 0.01f);
        }

        if (_instantiator == true)
        {
            Invoke("Reaparecer", 0.5f);
        }
    }
    public void RestablecerAceite()
    {
        _tiempoAceite = 1f;
    }
    public void RestablecerUsoPowerUp()
    {

    }
    public void Onturbo()
    {
        if (_turboOn == true)
        {
            _IAmovimiento.MaxForce += 0.5f;
        }
    }
    public void OffTurbo()
    {
        _IAmovimiento.MaxForce -= 0.5f;
    }
    private IEnumerator DoTurbo(float time)
    {
        yield return new WaitForSeconds(0.5f);
        _powerUpOn = true;
        yield return new WaitForSeconds(1.5f);
        _powerUpOn = false;
        _RegaloOn = false;
        _IAmovimiento.MaxForce = 81f;
        //RestablecerBoost();
    }
    private void DoCajaBomba()
    {
        _powerUpOn = true;
    }
    private IEnumerator DoShield(float tema)
    {
        yield return new WaitForSeconds(0.5f);
        _powerUpOn = true;
    }
    private IEnumerator TerminarEscudo(float temu)
    {
        yield return new WaitForSeconds(3f);
        _shieldOn = false;
        _Shield.enabled = false;
        _RegaloOn = false;
        _powerUpOn = false;
    }
    private IEnumerator BoostPistaOn(float taimu)
    {
        yield return new WaitForSeconds(1f);
        _turboOn = false;
        _IAmovimiento.MaxForce = 81f;
    }
    public void caidaBarranco()
    {
        //_semaforo._controlsOn = false;
        _rigidbody2d.gravityScale = 100;
        //_rigidbody2d.velocity = .normalized * _vBarranco;
    }
    public void Reaparecer()
    {
        transform.position = _instantiation.transform.position;
        _caidaOn = false;
        _spriteRenderer.enabled = true;
        //_semaforo._controlsOn = true;
        _salidaOnBarranco = false;
    }
    public void CocheCaer()
    {
        _spriteRenderer.enabled = false;
        _rigidbody2d.velocity = Vector2.zero;
        _rigidbody2d.gravityScale = 0;
    }
    private void QuitarMisil()
    {
        _misilOn = false;
    }
    private void QuitarCaja()
    {
        _cajaBombaOn = false;
    }
}
