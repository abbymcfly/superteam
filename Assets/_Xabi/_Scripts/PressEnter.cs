﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class PressEnter : MonoBehaviour {

    public GameObject _pressEnterPanel;
    public GameObject _p1P2Panel;


	void Start ()
    {

	}
	

	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            _pressEnterPanel.SetActive(false);
            _p1P2Panel.SetActive(true);
        }
        
    }
    public void Jugador1()
    {
        SceneManager.LoadScene(4);
    }
    public void Jugador2()
    {
        SceneManager.LoadScene(5);
    }
}
