﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PosicionSpritesIA : MonoBehaviour {
   
    //array para guardar los sprites
    public Sprite[] _arraySprites;
    public Transform _coche;
    public SpriteRenderer _spriteRenderer;
    private int _spriteActual;
    
    


    void Start ()
    {
       
	}

	void Update ()
    {
        Sprite spriteNuevo = _spriteRenderer.sprite;

        if (_coche.rotation.eulerAngles.z >= 0 && _coche.rotation.eulerAngles.z <= 23)
        {
            spriteNuevo = _arraySprites[0];
        }
        if (_coche.rotation.eulerAngles.z >= 24 && _coche.rotation.eulerAngles.z <= 65)
        {
            spriteNuevo = _arraySprites[1];
        }
        if (_coche.rotation.eulerAngles.z >= 66 && _coche.rotation.eulerAngles.z <= 107)
        {
            spriteNuevo = _arraySprites[2];
        }
        if (_coche.rotation.eulerAngles.z >= 107 && _coche.rotation.eulerAngles.z <= 149)
        {
            spriteNuevo = _arraySprites[3];
        }
        if (_coche.rotation.eulerAngles.z >= 149 && _coche.rotation.eulerAngles.z <= 191)
        {
            spriteNuevo = _arraySprites[4];
        }
        if (_coche.rotation.eulerAngles.z >= 191 && _coche.rotation.eulerAngles.z <= 233)
        {
            spriteNuevo = _arraySprites[5];
        }
        if (_coche.rotation.eulerAngles.z >= 233 && _coche.rotation.eulerAngles.z <= 275)
        {
            spriteNuevo = _arraySprites[6];
        }
        if (_coche.rotation.eulerAngles.z >= 275 && _coche.rotation.eulerAngles.z <= 317)
        {
            spriteNuevo = _arraySprites[7];
        }


        if (_spriteRenderer.sprite != spriteNuevo)
        {
            _spriteRenderer.sprite = spriteNuevo;
        }

       


    }

    private void FixedUpdate()
    {
        transform.rotation = Quaternion.Euler(Vector3.zero);
    }
}
