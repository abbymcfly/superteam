﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Semaforo : MonoBehaviour
{

    public GameObject _semaforo;
    public GameObject _rojo;
    public GameObject _naranja;
    public GameObject _verde;
    private float _tiempoSemaforo;
    public Rigidbody2D _semaforoRB;
    [HideInInspector]
    public float _tiempoEspera = 4f;
    public float _tiempoEsperaCoche = 4.5f;
    public bool _semaforoListo;
    public bool _SemaforoSeMueve = true;
    public bool _controlsOn = false;

    public void SemaforoJuego()
    {
        _tiempoSemaforo += Time.deltaTime;

        if (_tiempoSemaforo > 1f && _tiempoSemaforo < 2f)
        {
            _rojo.SetActive(true);
            _naranja.SetActive(false);
            _verde.SetActive(false);
        }
        if (_tiempoSemaforo > 2f && _tiempoSemaforo < 3f)
        {
            _rojo.SetActive(false);
            _naranja.SetActive(true);
            _verde.SetActive(false);
        }
        if (_tiempoSemaforo > 3f && _tiempoSemaforo < 4f)
        {
            _rojo.SetActive(false);
            _naranja.SetActive(false);
            _verde.SetActive(true); 
        }
    }

   

    private void Update()
    {
      
        //Vector3 newPosition = transform.position;
        //newPosition.y = _objetoSeMueve.position.y;
        //newPosition.y = Mathf.Clamp(newPosition.y, _puntoUno.position.y,_puntoDos.position.y);
        //newPosition = transform.position;
        if (_SemaforoSeMueve == true)
        {
            _semaforoRB.velocity = new Vector2(0, -250f);
        }else
        {
            _semaforoRB.velocity = new Vector2(0, 0f);
        }

        if (_semaforoListo == true)
        {
            Debug.Log(_tiempoEspera);
            SemaforoJuego();
            _tiempoEspera -= Time.deltaTime;
            _tiempoEsperaCoche -= Time.deltaTime;
        }
        
        if (_tiempoEspera <= 0)
        {
            _tiempoEspera = 0;
            
        }
        if (_tiempoEsperaCoche <= 0)
        {
            _controlsOn = true;

        }

        if (_tiempoEspera <= 0)
        {
            _semaforoRB.velocity = new Vector2(0, -250f);
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "TriggerSemaforo")
        {
            _SemaforoSeMueve = false;
            _semaforoListo = true;
            //_semaforoRB.velocity = new Vector2(0, 0);
        }
    }

   

}
