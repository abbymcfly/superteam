﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Misil : MonoBehaviour
{
    
    private PlayerPositionController _positionController;
    private GameObject Objetivo;
    public GameObject _misil;
   
    private SpriteRenderer _spriteRenderer;
    public Sprite _explosion;
    public bool _hitOn;
    public bool _hitOnIA;
    public Vector3 _escalaInicial;
    public Vector3 _escalaAumentada;
    public UnitySteer2D.Behaviors.AutonomousVehiclemisil _aVM;
    public CircleCollider2D _pc2D;
    private PolygonCollider2D _PC;
    public CircleCollider2D _pc2D2;
    public CarMovement _cm;
    public Animator _anim;
    public Animator _animIA;
    public Transform _playerTransform;
    public float _speed;
    public float _rotationSpeed;
    [HideInInspector] public string _parentName;
    public UnitySteer2D.Behaviors.AutonomousVehicle2D _AV2DIA;
    public IAEfectosMisil _IAMisil;

    void Start()
    {
        _positionController = GameObject.FindObjectOfType<PlayerPositionController>();
        _PC = GetComponent<PolygonCollider2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _escalaInicial = transform.localScale;
        _escalaAumentada = _escalaInicial * 4;
        _pc2D.enabled = false;
        _PC.enabled = false;
        _pc2D2.enabled = false;
        StartCoroutine(DevolverCollider(1f));
    }

    private IEnumerator  DevolverCollider (float tem)
    {
        yield return new WaitForSeconds(0.5f);
        _pc2D.enabled = true;
        _PC.enabled = true;
        _pc2D2.enabled = true;
    }

    public void IniciarMisil(int checkpoint, string parentName)
    {
        _parentName = parentName;
        Objetivo = _positionController.objetivoMisil(checkpoint);
    }
    
    private void Update()
    {
        if (_playerTransform != null)
        {
            Vector2 bombToPollo = _playerTransform.position - transform.position;
            Debug.Log(bombToPollo.sqrMagnitude);
            GetComponent<UnitySteer2D.Behaviors.AutonomousVehiclemisil>().enabled = false;
            float rotationZ = Mathf.Atan2(bombToPollo.y, bombToPollo.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(new Vector3(0f, 0f, rotationZ - 90)), 90f * _rotationSpeed * Time.deltaTime);
            Ray2D r = new Ray2D(transform.position, bombToPollo);
            Vector2 point = r.GetPoint(_speed);
            point = transform.InverseTransformPoint(point);
            transform.position += transform.up * _speed * Time.deltaTime;

        }


        if (_hitOn == true )
        {
            transform.localScale = Vector3.Lerp(transform.localScale, _escalaAumentada, Time.deltaTime * 6);
            _cm._misilOn = true;
            _anim.enabled = true;
            _playerTransform = null;
            StartCoroutine(DestruirMisil(1f));
            _pc2D.enabled = false;
            StartCoroutine(DesactivarSR(1f));
        }
        if (_hitOnIA == true)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, _escalaAumentada, Time.deltaTime * 6);
            _pc2D.enabled = false;
            _AV2DIA.enabled = true;
            _playerTransform = null;
            StartCoroutine(DestruirMisilIA(1f));
            _animIA.enabled = true;
            _AV2DIA._misilOn = true;
            StartCoroutine(DesactivarSRIA(1f));
        }
    }

   

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Jugadores")
        {
            _cm = collision.gameObject.GetComponent<CarMovement>();
            Rigidbody2D r2d = collision.gameObject.GetComponent<Rigidbody2D>();
             _anim = collision.gameObject.GetComponent<Animator>();
            r2d.AddForceAtPosition(collision.contacts[0].normal * -500, collision.contacts[0].point);
            _spriteRenderer.sprite = _explosion;
            _hitOn = true;
            _aVM.enabled = false;
        }
        if (collision.gameObject.tag == "IA")
        {
            Rigidbody2D r2dIA = collision.gameObject.GetComponent<Rigidbody2D>();
            _animIA = collision.gameObject.GetComponent<Animator>();
            _IAMisil = collision.gameObject.GetComponent<IAEfectosMisil>();
            r2dIA.AddForceAtPosition(collision.contacts[0].normal * -0.5f, collision.contacts[0].point);
            _spriteRenderer.sprite = _explosion;
            _hitOnIA = true;
            _aVM.enabled = false;
            _AV2DIA = collision.gameObject.GetComponentInParent<UnitySteer2D.Behaviors.AutonomousVehicle2D>();
            _AV2DIA.enabled = false;
            _IAMisil._misilOn = true; 
        }
        if (collision.gameObject.tag == "Obstaculo")
        {
            _PC.isTrigger = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Obstaculo")
        {
            _PC.isTrigger = false;
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.gameObject.tag == "Jugadores") && _parentName != collision.gameObject.name)
        {
            _playerTransform = collision.transform;
        }
        if ((collision.gameObject.tag == "IA") && _parentName != collision.gameObject.name)
        {
            _playerTransform = collision.transform;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //Comentamos esto porque si lo dejamos descomentado el misil no sigue apropiadamente a los jugadores
        //_playerTransform = null;
        GetComponent<UnitySteer2D.Behaviors.AutonomousVehiclemisil>().enabled = true;
    }

    private IEnumerator DestruirMisil(float tum)
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
    private IEnumerator DestruirMisilIA(float tam)
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
    private IEnumerator DesactivarSR (float tem)
    {
        yield return new WaitForSeconds(0.5f);
        _spriteRenderer.enabled = false;
        _cm._misilOn = false;
        _anim.enabled = false;
    }
    private IEnumerator DesactivarSRIA(float tem)
    {
        yield return new WaitForSeconds(0.5f);
        _spriteRenderer.enabled = false;
        //_AV2DIA._misilOn = false;
        //_animIA.enabled = false;
        //_AV2DIA._misilOn = false;
    }
}
