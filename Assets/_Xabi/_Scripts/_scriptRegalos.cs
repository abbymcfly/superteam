﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _scriptRegalos : MonoBehaviour
{
    public bool _RegaloOn;
    public bool _RegaloOnIA;
    [HideInInspector]
    public float _tiempoReaparicion = 0.5f;
    private GameObject _this;
    private SpriteRenderer _spriteRenderer;
    private BoxCollider2D _Collider;
    public Regalos jugador;
    public IAEfectosCarrera IARegalos;

    private void Start()
    {
        _this = GetComponent<GameObject>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _Collider = GetComponent<BoxCollider2D>();
    }
    void Update()
    {

        // -----------JUGADOR------------

        if (_RegaloOn == true)
        {
            _tiempoReaparicion -= Time.deltaTime;


            if (_tiempoReaparicion <= 0)
            {
                //jugador._RegaloOn = false;
                _spriteRenderer.enabled = true;
                _Collider.enabled = true;
                _RegaloOn = false;
                _tiempoReaparicion = 0.5f;
            }
        }
        //----------- IA --------------

        if (_RegaloOnIA == true)
        {
            _tiempoReaparicion -= Time.deltaTime;


            if (_tiempoReaparicion <= 0)
            {
                //IARegalos._RegaloOn = false;
                _spriteRenderer.enabled = true;
                _Collider.enabled = true;
                _RegaloOnIA = false;
                _tiempoReaparicion = 0.5f;
            }
        }
    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        // Jugadores ---------------------------------------
        if (collision.gameObject.tag == "Jugadores")
        {
            jugador = collision.gameObject.GetComponent<Regalos>();
            if (jugador._RegaloOn == true)
            {
                // ya tengo regalo no tengo que cogerlo
            }
            else
            {
                // no tengo regalo, tengo que cogerlo
                _RegaloOn = true;
                jugador._RegaloOn = true;
                jugador.CalcularRegalo();
                _spriteRenderer.enabled = false;
                _Collider.enabled = false;
                
            }

        }
        // IA ----------------------------------------

        if (collision.gameObject.tag == "IA")
        {
            IARegalos = collision.gameObject.GetComponent<IAEfectosCarrera>();
            if (IARegalos._RegaloOn)
            {

            }
            else
            {
                IARegalos._RegaloOn = true;
                _spriteRenderer.enabled = false;
                _Collider.enabled = false;
                _RegaloOnIA = true;
            }
        }
    }
}

