﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CocheActivo : MonoBehaviour {

    public GameObject _alpha;
    public GameObject _OCP;
    public GameObject _delorean;
    public GameObject _luigi;
    public GameObject _mario;

    

    void Start ()
    {
		if(Preferencias._coche1 == "Alpha")
        {
            _alpha.SetActive(true);
            _OCP.SetActive(false);
            _delorean.SetActive(false);
            _luigi.SetActive(false);
            _mario.SetActive(false);

        }

        if (Preferencias._coche2 == "Alpha")
        {
            _alpha.SetActive(true);
            _OCP.SetActive(false);
            _delorean.SetActive(false);
            _luigi.SetActive(false);
            _mario.SetActive(false);

        }
        if (Preferencias._coche1 == "OCP")
        {
            _alpha.SetActive(false);
            _OCP.SetActive(true);
            _delorean.SetActive(false);
            _luigi.SetActive(false);
            _mario.SetActive(false);

        }

        if (Preferencias._coche2 == "OCP")
        {
            _alpha.SetActive(false);
            _OCP.SetActive(true);
            _delorean.SetActive(false);
            _luigi.SetActive(false);
            _mario.SetActive(false);

        }
        if (Preferencias._coche1 == "Delorean")
        {
            _alpha.SetActive(false);
            _OCP.SetActive(false);
            _delorean.SetActive(true);
            _luigi.SetActive(false);
            _mario.SetActive(false);

        }

        if (Preferencias._coche2 == "Delorean")
        {
            _alpha.SetActive(false);
            _OCP.SetActive(false);
            _delorean.SetActive(true);
            _luigi.SetActive(false);
            _mario.SetActive(false);

        }
        if (Preferencias._coche1 == "Ekaitz")
        {
            _alpha.SetActive(false);
            _OCP.SetActive(false);
            _delorean.SetActive(false);
            _luigi.SetActive(true);
            _mario.SetActive(false);

        }

        if (Preferencias._coche2 == "Ekaitz")
        {
            _alpha.SetActive(false);
            _OCP.SetActive(false);
            _delorean.SetActive(false);
            _luigi.SetActive(true);
            _mario.SetActive(false);

        }

    }
	
	
	void Update ()
    {
		
	}
}
