﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escudo : MonoBehaviour
{
    private CarMovement _cm;
    public bool _triggerOff;
    private Regalos _regalos;
    private Collider2D _collider;
    private RegaloBomba _bomba;


    // Use this for initialization
    void Start()
    {
        _cm = GetComponent<CarMovement>();
        _regalos = GetComponent<Regalos>();
        _collider = GetComponent<Collider2D>();
        
     
    }
    private void Update()
    {
        if (_triggerOff == true)
        {
            StartCoroutine("DejarDeSerTrigger");
        }
        if (_triggerOff == true)
        {
            StartCoroutine("ResetearTrigger");
        }
        if ( !_cm._OnePlayerOnly && _cm._caidaOn == true )
        {
            _regalos._Shield.enabled = false;
        }
        

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "RegaloBomba")
        {
            _collider.isTrigger = true;
            _triggerOff = true;
        }
    }
    private IEnumerator DejarDeSerTrigger()
    {
        yield return new WaitForSeconds(1f);
        _collider.isTrigger = false;
    }
    private IEnumerator ResetearTrigger()
    {
        yield return new WaitForSeconds(2f);
        _triggerOff = false;
    }

}
