﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorCheckPoint : MonoBehaviour
{
    private int indice = 0;
    //public float _tiempoPartida;
    //public int _tiempoRecord;

        // VARIABLES QUE INDICAN EN QUÉ VUELTA Y EN QUÉ CHECKPOINT ESTÁ ACTUALMENTE
    public int _lapNumber = 1;
    public int _position;
    public int _actualCheckPoint;
    public Text _LapNumberText; //aqui declaro la variable para que salga la vuelta en el canvas
    public Text _PositionText;
    public bool _IAOn;
    public GameObject _semaforo;
    public Semaforo _semaforoScript;
    public GameObject _panelTerminarCarrera;
    public GameObject _J1;
    public GameObject _J2;
    public ControladorCheckPoint _ccpScript;
    public ControladorCheckPoint _ccpScript2;
    public CarMovement _cm1;
    public CarMovement _cm2;
    public Regalos _rg;
    public GameObject _IA;
    public GameObject _panelTerminaCarreraJ1;
    public GameObject _panelTerminaCarreraJ2;
    public Text _metaJ1;
    public Text _posicionFinalJ1;
    public Text _metaJ2;
    public Text _posicionFinalJ2;
    public Image _imagePanel1;
    public Image _imagePanel2;
    public bool _OnePlayer;
   

    private PlayerPositionController _ppc;
 

    

    // Use this for initialization
    void Start()
    {
        

        if (!_IAOn && _cm1._OnePlayerOnly)
        {
            _rg = GetComponent<Regalos>();
        }
        if (!_IAOn && _OnePlayer == true && !_cm1._OnePlayerOnly)
        {
            _J1 = GameObject.Find("J1");
            _imagePanel1 = _panelTerminaCarreraJ1.gameObject.GetComponent<Image>();
            _rg = GetComponent<Regalos>();
            _ccpScript = _J1.GetComponent<ControladorCheckPoint>();
            _cm1 = _J1.GetComponent<CarMovement>();
            _posicionFinalJ1 = GameObject.Find("Posición Final J1").GetComponent<Text>();
            _metaJ1 = GameObject.Find("meta J1").GetComponent<Text>();
        }
        
        _panelTerminaCarreraJ1 = GameObject.Find("Terminar Carrera Antes J1");
        if (!_OnePlayer && !_IAOn && !_cm1._OnePlayerOnly)
        {
            _J2 = GameObject.Find("J2");    
            _panelTerminaCarreraJ2 = GameObject.Find("Terminar Carrera Antes J2");
            _posicionFinalJ2 = GameObject.Find("Posición Final J2").GetComponent<Text>();
            _metaJ2 = GameObject.Find("meta J2").GetComponent<Text>();
            _imagePanel2 = _panelTerminaCarreraJ2.gameObject.GetComponent<Image>();
            _ccpScript2 = _J2.GetComponent<ControladorCheckPoint>();
            _cm2 = _J2.GetComponent<CarMovement>();
        }
       
     
       
        
        //_tiempoPartida = 0f;
        _ppc = FindObjectOfType<PlayerPositionController>();
        _semaforo = GameObject.Find("semaforo_off");
        //_semaforoScript = _semaforo.GetComponent<Semaforo>();

        if (!_IAOn)
        {
            if (_rg._numeroDeJugador == 1)
            {
                _LapNumberText = GameObject.Find("Lap Jugador 1").GetComponent<Text>();
                _PositionText = GameObject.Find("Posicion Jugador 1").GetComponent<Text>();
            
             if (_OnePlayer && !_IAOn && !_cm1._OnePlayerOnly)
                {
                    _metaJ1.enabled = false;
                    _posicionFinalJ1.enabled = false;
                    _imagePanel1.enabled = false;
                }
            }
            if (!_OnePlayer && _rg._numeroDeJugador == 2 && !_IAOn && !_cm1._OnePlayerOnly)
            {
                _LapNumberText = GameObject.Find("Lap Jugador 2").GetComponent<Text>();
                _PositionText = GameObject.Find("Posicion Jugador 2").GetComponent<Text>();

                if (!_OnePlayer && !_IAOn && !_cm1._OnePlayerOnly)
                {
                    _metaJ2.enabled = false;
                    _posicionFinalJ2.enabled = false;
                    _imagePanel2.enabled = false;
                }
            }   
        }
    }

    // Update is called once per frame
    void Update()
    {
       
        // _tiempoPartida += Time.deltaTime;
        //Linea de Xabi. FUCIONAA!!a cada jugador hay que ponerle su text
        if (!_IAOn)
        {
            _LapNumberText.text = ("Lap " + _lapNumber.ToString());
            _PositionText.text = _ppc.GetPosition(this);
        }




    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        
        
        if (collision.gameObject.tag == "checkpoint")
        {
            int indiceCheckpoint = int.Parse(collision.gameObject.name);
            if (indiceCheckpoint == 1 && _actualCheckPoint == 7)
            {
                // REINICIO EL CONTADOR DE CHECKPOINTS
                _actualCheckPoint = indiceCheckpoint;
                // TENGO QUE SUMAR UNA VUELTA AL JUGADOR
                _lapNumber += 1;
                //LINEA DE XABI
                if (!_OnePlayer && _IAOn && !_cm1._OnePlayerOnly)
                {
                    if (_ccpScript._lapNumber == 5 && !_IAOn)
                    {
                        _cm1._terminarCarrera = true;
                        _metaJ1.enabled = true;
                        _posicionFinalJ1.text = ("Has quedado en la posición " + _ppc.GetPosition(this).ToString());
                        _posicionFinalJ1.enabled = true;
                        _imagePanel1.enabled = true;

                    }
                }
                if (!_OnePlayer && !_IAOn && !_cm1._OnePlayerOnly)
                {
                    if (_ccpScript2._lapNumber == 5)
                    {
                        _cm2._terminarCarrera = true;
                        _metaJ2.enabled = true;
                        _posicionFinalJ2.text = ("Has quedado en la posición " + _ppc.GetPosition(this).ToString());
                        _posicionFinalJ2.enabled = true;
                        _imagePanel2.enabled = true;
                    }
                    if (!_cm1._OnePlayerOnly)
                    {
                        if (_ccpScript._lapNumber == 5 && _ccpScript2._lapNumber == 5)
                        {
                            _semaforoScript._controlsOn = false;
                            _panelTerminarCarrera.SetActive(true);
                        }
                    }
                    else if (_ccpScript._lapNumber == 5)
                    {
                        _semaforoScript._controlsOn = false;
                        _panelTerminarCarrera.SetActive(true);
                    }
                }
            }
            else if (indiceCheckpoint == _actualCheckPoint + 1)
            {
                // indice = indice + 1;
                // PONEMOS EL NÚMERO ACTUAL DEL CHECKPOINT POR EL QUE VA EL JUGADOR
                // AL NÚMERO DEL CHECKPOINT POR EL QUE HA PASADO
                _actualCheckPoint = indiceCheckpoint;
            }
            
           // if (indice == 24)
            //{
              //  Debug.Log("has ganado");
                
               //float _tiempoRecordAnterior = PlayerPrefs.GetFloat("NumeroEntero", 5000f);
               // if (_tiempoRecordAnterior > _tiempoPartida)
               // {
               //     PlayerPrefs.GetFloat("NumeroEntero");
               // }
               // Debug.Log(_tiempoRecordAnterior);
            }
        }
    }
    
