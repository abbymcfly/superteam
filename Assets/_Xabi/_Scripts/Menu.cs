﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    public GameObject _menuSingleMulti;
    public GameObject _menuDificultad;

    void Start ()
    {
		
	}
	
	
	void Update ()
    {

        if (_menuSingleMulti.activeInHierarchy == true && Input.GetKeyDown(KeyCode.Space))
        {
            _menuSingleMulti.SetActive(false);
            _menuDificultad.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            _menuSingleMulti.SetActive(true);
        }
        
    }
}
