﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tiempo : MonoBehaviour
{
    public Text _tiempoText;
    public float _tiempo = 0f;
    public Semaforo _semaforo;
    



    void Update ()
    {
        if(_semaforo._controlsOn == true)

        _tiempo += Time.deltaTime;
        TimeSpan timeSpan = TimeSpan.FromSeconds(_tiempo);
        _tiempoText.text = string.Format("{0}'{1}''{2}", timeSpan.Minutes, timeSpan.Seconds.ToString("00"), timeSpan.Milliseconds.ToString());
       
    }
}
