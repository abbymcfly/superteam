﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegaloBomba : MonoBehaviour {
    private Escudo _Shield;
    //Declaramos el spriteRenderer para cambiarlo cuando explote
    private SpriteRenderer _spriteRenderer;
    // Declaramos el sprite por el que se cambiará la caja al explotar
    public Sprite _explosion;
    // Declaramos la fuerza de explosión que tendrá la caja al explotar
    public float _fuerzaExplosion = 999f;
    // Declaramos el radio de la explosión
    public float _radioExplosion = 3f;
    // Declaramos una variable que les devolverá el control sobre el vehículo tanto para el jugador como la IA
    public bool _fin;
    public bool _finIA;
    //Declaramos los scripts de la IA y el jugador para arrebatarles los controles del vehículo en el momento de la explosión
    private CarMovement _cm;
    private EfectosBombaIA _AV2D;
    //Declaramos la variable hitOn que desencadenará el efecto de la explosión
    public bool _hitOn;
    //Declaramos las dos escalas de la explosión para darle el efecto de ir aumentando
    public Vector3 _escalaInicial;
    public Vector3 _escalaAumentada;
    private Collider2D _collider;
    public GameObject _cocheChoque;

	// Use this for initialization
	void Start ()
    {
        // Recuperamos el Collider2D
        _collider = GetComponent<Collider2D>();
        // Desactivamos el collider al empezar para que no nos de a nosotros nada más tirar la caja bomba
        _collider.enabled = false;
        // Recuperamos el componente SpriteRenderer y declaramos las dos escalas
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _escalaInicial = transform.localScale;
        _escalaAumentada = _escalaInicial * 3;
        // Hacemos que el collider se active 0.05 segundos después de tirar la caja
        Invoke("ActivarCollider", 1f);
    }
	
	// Update is called once per frame
	void Update ()
    {
        
        // Cuando _hitOn sea true, el sprite de la explosión "crecerá" dandole el efecto de explosión
		if (_hitOn == true)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, _escalaAumentada, Time.deltaTime * 6);
        }
        //Cuando la variable _fin sea true al jugador se le devolverá el control del vehículo
        if (_fin == true)
        {
            _cm._BombPU = false;
        }
        //Cuando la variable _finIA sea true a la IA se le devolverá el control del vehículo
        if (_finIA == true)
        {
            _AV2D._BombPUIA = false;
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        // --------- JUGADORES ---------

        // Si chocamos con un jugador...
        if (collision.gameObject.tag == "Jugadores")
        {
            
            //Debug.Log("EXPLOTASTE WEY");
            //Si el jugador choca contra la caja, el spriteRenderer de la caja cambia y sale la explosión
            _spriteRenderer.sprite = _explosion;
            
            //Hacemos que la variable _hitOn sea true para que la explosión haga el efecto de agrandarse
            _hitOn = true;
            // En cuando colisionamos con el jugador cogemos su Rigidbody y el Script CarMovement 
            Rigidbody2D r2d = collision.gameObject.GetComponent<Rigidbody2D>();
             _cm = collision.gameObject.GetComponent<CarMovement>();
            Animator _anim = collision.gameObject.GetComponent<Animator>();
            _Shield = collision.gameObject.GetComponent<Escudo>();
            // Una vez hemos pillado el rigidbody debido a la colisión, lo usamos para aplicarle una fuerza de impacto
            r2d.AddForceAtPosition(collision.contacts[0].normal * -500f, collision.contacts[0].point);
            // Al mismo tiempo que le damos fuerza de impacto le quitamos los controles sobre el vehículo
            _cm._BombPU = true;
            // Comenzamos la corrutina a la que le hemos dicho que esperase 0.2 segundos tras la explosión
            StartCoroutine("DevolverControlesPlayer");
            // 0.3 Segundos tras la explosión, destruimos todo el gameObject de la caja
            Invoke("DestruirCaja", 0.3f);
            /*
            Rigidbody2D _carRb = collision.GetComponent<Rigidbody2D>();
            Vector2 dir = (new Vector3(_carRb.position.x, _carRb.position.y) - transform.position);
            float wearoff = 1 - (dir.magnitude / _radioExplosion);
            _carRb.AddForce(dir.normalized * _fuerzaExplosion * wearoff);
            */
        }

        // ----------------- IA -------------------

        // Si chocamos con la IA...
        if (collision.gameObject.tag == "IA")
        {
            //Debug.Log("EXPLOTASTE WEY");
            _spriteRenderer.sprite = _explosion;

            _hitOn = true;

            Rigidbody2D r2dIA = collision.gameObject.GetComponent<Rigidbody2D>();

            r2dIA.AddForceAtPosition(collision.contacts[0].normal * -5000f, collision.contacts[0].point);

            _AV2D = collision.gameObject.GetComponentInChildren<EfectosBombaIA>();

            _AV2D._BombPUIA = true;

            StartCoroutine("DevolverControlesIA");

            Invoke("DestruirCaja", 0.3f);    
        }
    }
    // Declaramos el void que devolverá el collider a la caja bomba
    public void ActivarCollider()
    {
        _collider.enabled = true;
    }
    // Declaramos las corrutinas que devolverán los controles al jugador y la IA
    public IEnumerator DevolverControlesPlayer()
    {
        yield return new WaitForSeconds(2f);
        _fin = true;
    }
    public IEnumerator DejarDeSerTrigger(float tem)
    {
        yield return new WaitForSeconds(2f);
        _Shield._triggerOff = true;
    }
    
    public IEnumerator DevolverControlesIA()
    {
        yield return new WaitForSeconds(2f);
        _finIA = true;  
    }
    // Declaramos el void que destruirá la caja tras la explosión
    public void DestruirCaja()
    {
        Destroy(gameObject); 
    }
   
    
   
}
