﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EfectosBombaIA : MonoBehaviour {
    public IAEfectosCarrera _IA2;
    public bool _BombPUIA = false;
    private UnitySteer2D.Behaviors.AutonomousVehicle2D _IA;
    // Use this for initialization
    void Start ()
    {
        _IA = GetComponent<UnitySteer2D.Behaviors.AutonomousVehicle2D>();
        _IA2 = GetComponentInChildren<IAEfectosCarrera>();
	}
	
	// Update is called once per frame
	void Update ()
   {
        if (_IA2._shieldOn == true)
        {
            _BombPUIA = false;
        }

        if (_BombPUIA == true)
        {
            _IA.MaxForce = 0;
            _IA.MaxSpeed = 0;   
            StartCoroutine("QuitarBombPUIA");
        }
    }
    private IEnumerator QuitarBombPUIA()
    {
        yield return new WaitForSeconds(2f);
        _BombPUIA = false;
        _IA.MaxForce = 81;
        _IA.MaxSpeed = 1000;
    }

}
