﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollidersPista : MonoBehaviour
{
    private PolygonCollider2D _pc2D;
    private bool _haPasado;
    // Use this for initialization
    void Start()
    {
        _pc2D = GetComponent<PolygonCollider2D>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Misil")
        {
            _pc2D.isTrigger = true;
             _haPasado = true;
        }
    }
    private void Update()
    {
        if (_haPasado == true)
        {
            StartCoroutine(QuitarTrigger(0.1f));
        }
    }
    private IEnumerator QuitarTrigger (float tem)
    {
        yield return new WaitForSeconds(0.1f);
        _pc2D.isTrigger = false;
        _haPasado = false;
    }

}
