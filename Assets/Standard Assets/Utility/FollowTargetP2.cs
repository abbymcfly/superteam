using System;
using UnityEngine;


namespace UnityStandardAssets.Utility
{
    public class FollowTargetP2 : MonoBehaviour
    {
        public Transform target;
        public Vector3 offset = new Vector3(0f, 7.5f, 0f);
        /*public GameObject _coche1;
        public GameObject _coche2;
        public GameObject _coche3;
        public CarMovement _cm;
        public CarMovement _cm2;
        public CarMovement _cm3;
        public Transform _coche1T;
        public Transform _coche2T;
        public Transform _coche3T;*/

        /*void Start()
        {
            _coche1 = GameObject.Find("Alpha J2");
            _coche2 = GameObject.Find("Mario J2");
            _coche3 = GameObject.Find("Coche de policia J2");
            _cm = _coche1.GetComponent<CarMovement>();
            _cm2 = _coche2.GetComponent<CarMovement>();
            _cm3 = _coche3.GetComponent<CarMovement>();
            _coche1T = _coche1T.transform.Find("Alpha J2");
            _coche2T = _coche1T.transform.Find("Mario J2");
            _coche3T = _coche1T.transform.Find("Coche de policia J2");

        }
        void Update()
        {
            if (_cm._playerNumber == 2)
            {
                target = _coche1T;
            }
            else if (_cm2._playerNumber == 2)
            {
                target = _coche2T;
            }
            else if (_cm3._playerNumber == 2)
            {
                target = _coche3T;
            }

        }*/
        private void LateUpdate()
        {
            transform.position = target.position + offset;
          
        }
    }
}
