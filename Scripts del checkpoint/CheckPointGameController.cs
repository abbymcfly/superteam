﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CheckPointGameController : MonoBehaviour {

    // Declaramos todos los Animators que vamos
    // a necesitar durante el juego
    [Header("Animators")]
    public Animator _checkPointAnimator;
    public Animator _wrongCheckPointAnimator;
    public Animator _raceCompletedAnimator;
    public Animator _outOfTimeAnimator;

    // Declaramos todos los sonidos que vamos
    // a necesitar durante el juego
    [Header("Sounds")]
    public AudioSource _audioSource;
    public AudioClip _checkPointClip;
    public AudioClip _wrongCheckPointClip;
    public AudioClip _outOfTimeClip;
    public AudioClip _raceCompletedClip;

    // Declaramos el resto de variables necesarias
    // para controlar el juego
    [Header("Game Settings")]
    public float _gameTime; // Indica el tiempo que nos queda
    public float _checkPointTime; // Indica el tiempo que da cada checkpoint
    private bool _gameEnded; // Indica si hemos terminado el juego 
    public Text _timeText; // La caja de texto para el tiempo
    private int _currentCheckPoint; // Indica en que checkpoint estamos
    private int _checkPointCount; // Indica la cantidad total de checkpoints

	// Use this for initialization
	void Start ()
    {
        // Recuperamos del juego todos los checkpoints
        CheckPoint[] allCheckPoints = FindObjectsOfType<CheckPoint>();
        // Contamos todos los checkpoints
        _checkPointCount = allCheckPoints.Length;
        // Nos suscribimos al evento de entrar en el checkpoint
        CheckPoint.OnEnter += CheckPoint_OnEnter;
	}

    private void CheckPoint_OnEnter(int checkPointNumber, bool alreadyEntered)
    {
        // Cubrimos el caso en el que hemos llegado
        // al último checkpoint
        if (_currentCheckPoint + 1 == _checkPointCount && _currentCheckPoint + 1 == checkPointNumber)
        {
            // Lanzamos el animator de carrera completada
            _raceCompletedAnimator.SetTrigger("CheckPoint");
            // Repoducimos el sonido de carrera completada
            _audioSource.PlayOneShot(_raceCompletedClip);
            // Establecemos que hemos terminado el juego
            _gameEnded = true;
            // Llamamos a la corutina para recargar
            // la escena
            StartCoroutine(ReloadGame());
        }
        // Cubrimos el caso en el que pasamos por un checkpoint
        // opr primera vez
        else if (!alreadyEntered && checkPointNumber == _currentCheckPoint + 1)
        {
            // Lanzamos el animator del checkpoint correcto
            _checkPointAnimator.SetTrigger("CheckPoint");
            // Establecemos la variable que nos indica en que checkpoint
            // estamos
            _currentCheckPoint = checkPointNumber;
            // Sumamos el tiempo ganado por el checkpoint
            _gameTime += _checkPointTime;
            // Reproducimos el sonido de checkpoint correcto
            _audioSource.PlayOneShot(_checkPointClip);
        }
        // Cubrimos el último caso que es cuando
        // intentamos pasar por un checkpoint
        // que ya hemos pasado
        else
        {
            // Lanzamos el animator de que hemos pasado
            // por un checkpoint incorrecto
            _wrongCheckPointAnimator.SetTrigger("CheckPoint");
            // Reproducimos el audio de que nos hemos
            // equivoado de checkpoint
            _audioSource.PlayOneShot(_wrongCheckPointClip);
        }
    }

    // Update is called once per frame
    void Update ()
    {
		// Comprobamos si hemos terminado el juego
        if (!_gameEnded)
        {
            // Restamos el tiempo al tiempo total
            _gameTime -= Time.deltaTime;
            // Mostramos por pantalla el tiempo restante
            _timeText.text = Mathf.RoundToInt(_gameTime).ToString();
            // Comprobar si se ha terminado el tiempo
            if (_gameTime <= 0f)
            {
                // Lanzamos el trigger que muestra el texto
                // de que nos hemos quedado sin tiempo
                _outOfTimeAnimator.SetTrigger("CheckPoint");
                // Reproducir el sonido de que nos hemos
                // quedado sin tiempo
                _audioSource.PlayOneShot(_outOfTimeClip);
                // Establecemos la variable que indicia
                // que hemos terminado el juego
                _gameEnded = true;
                // Llamamos a la corutina que recargará
                // la escena en 4 segundos
                StartCoroutine(ReloadGame());
            }
        }
	}

    // Creamos el método que es una corutina
    // que se encarga de recargar la escena
    private IEnumerator ReloadGame()
    {
        // Le decimos al juego que espere
        // 4 segundos antes de recargar
        // la escena
        yield return new WaitForSeconds(4);
        // Recargamos la escena en la que estamos
        SceneManager.LoadScene(0);
    }
}
