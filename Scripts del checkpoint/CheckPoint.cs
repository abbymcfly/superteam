﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {

    // Creamos un delegado para el evento
    // en el que el jugador entra en el
    // el trigger del checkpoint
    public delegate void Entered(int checkPointNumber, bool alreadyEntered);
    // Creamos la definición del evento 
    // que se lanzará cuando el jugador entre
    // en el trigger
    public static event Entered OnEnter;

    // Declaramos una variable que nos indica
    // el orden del checkpoint
    public int _checkPointNumber;
    // Declaramos una variable que nos indica
    // si ya hemos pasado por el checkpoint
    private bool _alreadyEntered;


    private void OnTriggerEnter(Collider other)
    {
        // Comprobamos si ha chocado el jugador
        if (other.tag == "Player")
        {
            // Miramos si hay suscripciones al evento
            if (OnEnter != null)
            {
                // Lanzamos el evento
                OnEnter(_checkPointNumber, _alreadyEntered);
                // Establecemos la variable que indica
                // que hemos pasado por el checkpoint
                _alreadyEntered = true;
            }
        }
    }

    private void OnDestroy()
    {
        // Eliminamos todas las suscripciones al evento
        OnEnter = null;
    }
}
