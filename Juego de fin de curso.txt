Nombre del Juego:
 
- Mario Kart 2D


Caracteristicas:

- Posibilidad de elegir entre 6 personajes

- Multijugadro competitivo a pantalla partida

- 3 niveles de dificultad

- 5 circuitos diferentes(Casino, cementerio, monta�a nevada, ruinas incas, castillo)

- Hay 3 tipos de copa: Bronce, Plata, Oro. corresponden a la puntuacion final del      jugador al final de todos los circuitos. el que mas puntos tenga, se llevara la      copa de oro, el segundo que mas tenga, Plata y el tercero bronce

- en la vista del juego la cmaara si situara detras del coche, dando una apariencia   de 3D, tipo Super Mario Kart.

- posibilidad de juego online

- podras sacar una foto de tu cara y subirla para que sea la imagen del conductor



Mercado y Plataforma:

- Moviles y pc.

- todas las edades



Sinopsis:

- abby y sus amigos han decidido que ya esta bien de ir a clase en bus. han robado  
  unos karts y se han propuesto llegar a clase los primeros. abby, que es muy          marrullera, ha pensado que seria aun mas divertido, usar trampas para llegar         antes que los demas. no le sera tan facil porque los demas tambien son unos         marrulleros. 3, 2 , 1...GOO


Apariencia y ambientacion:

- juego en 2D con sprites tipo pixel art

- perspectiva isometrica

- una paleta de colores vivos para hacer mas simpatica la imagen de los personajes


Mecanicas de juego presentes:

- para ganar la carrera hay que llegar el primero a meta

- cada circuito dara una cantidad de puntos dependiendo de la posicion en la que       haya quedado el jugador

- posibilidad de atacar a otros jugadores con los objetos que se van recogiendo en     cada circuito, para ralentizarles, o para tener ventajas en nuestro kart.

- Habra unas "cajas" en el suelo que al pasar sobre ellas nos daran objetos. esos      objetos los podremos usar para conseguir ventajas en la carrera. solo podremos       pasar por encima de estas cajas, una vez por vuelta.

- los efectos de los objetos variaran dependiendo de estos

- Alterminar la carrera, el primero se llevara 5 puntos, segundo 4, tercero 2,       cuarto 1

- si la posicion del jugador esta por encima de la 4�, perdera una vida.

- si las vidas del jugador llegan a 0. el juego terminara

- si el jugador pulsa el boton de acelerar entre el primer color del semaforo y el     segundo, saldra con un turbo

- la salida la dara un semaforo. tendra 3 luces. al activarse la tercera, los coches   podran comenzar la carrera

- Habra 6 personajes en pantalla. 1 o 2 manejados por los jugadores y el resto por     la maquina


Definicion de los Menus:

- Habra una pantalla principal en la que aparecera un menu para elgir entre 1 o 2     jugadores, despues aparecera otro en el que dejara elegir el nivel de dificultad,    despues un tercero que preguntara si las opciones elegidas estan bien.
  Aparecera un segundo menu que dara a elegir los diferentes personajes.
  y un tercer menu que dejara elegir el circuito.

- si se pierde una carrera aparecera una ventana diciendote si quieres gastar una     vida o quieres volver a empezar el juego. si se empieza, pondra un game over y     volvera a la pantalla principal. si se gasta una vida, la carrera empezara de     nuevo.

- si terminas todos los circuitos, habra una escena donde se entregara la copa que     corresponda al personaje. pantalla de creditos y al terminar vuelta a la pantalla    principal.


Controles:

- 6 botones

- Acelerar, frenar, usar objeto, boton de start, cambio de vista, saltar/derrapar.


Informacion en pantalla durante el juego:

- en las carreras, en la parte superior derecha habra una cronometro que mostrara el   tiempo transcurrido.

- en el centro superior de la pantalla Habra un cuadrado para mostrar el objeto que    ha tocado

- abajo a la derecha de la pantalla tendremos nuestra cantidad de vidas y la     posicion en la que estamos en la carrera

- cuando lleguemos a la ultima vuelta saldra un aviso que nos dira que estamos en la   vuelta final.

- cuando nuestra posicion esta por encima de la 4� aparecera en rojo para indicarnos   que necesitamos subir puestos   

- cada vez terminemos una vuelta se nos indicara en la pantalla.



Movimiento e interaccion del personaje:

- el personaje solo podra moverse de izquierda a derecha

- en las curvas podra derrapar para tomarlas mejor

- nuestro coche podra chocar contra otros coches

- los coches no tienen salud. aunque los echen de la pista, les choquen, o les     golpeen, siempre estaran igual y se recuepraran pasados unos segundos. nunca       explotaran o perderan vidas asi.



Sonidos:

- musica del menu principal

- sonidos de menu desplegable cuando sale la opcion para 1 o 2 jugadores

- un sonido para cuando subes o bajas en el menu de 1 o 2 jugadores y de dificultad

- mismo sonido para la seleccion de dificultad

- y lo mismo para la confirmacion de las selecciones

- musica para la seleccion de vehiculo

- musica para la seleccion de circuito

- musica para cada carrera

- un sonido para cuando cambias de un coche a otro

- un sonido para cuando cambias de circuito

- musica para cuando ganas la carrera

- musica para cuando pierdes la carrera

- sonido para cuando gastas una vida

- sonido de derrape

- sonido de aceleracion

- sonido de freno
 
- sonido de seleccion de objetos dentro del juego

- sonido al usar un objeto

- sonido del semaforo de salida. un sonido para las dos primeras luces del semaforo   y otro para la ultima 

- un sonido para cuando te caes de la pantalla

- un sonido para cuando te chocas

- sonido para cuando pisas diferentes tipos de terreno

- sonido para cuando respawnea un coche

- musica para cuando quedas primero en todos los circuitos y ganas una copa

- musica de creditos


Objetos:

- Turbo: un portal oscuro que te lanza y aumenta tu velocidad en un     100% por 2 segundos

- guante de boxeo: un guante de boxeo que sale disparado y golpea al   rival, empujandolo y disminuyendo su velocidad en 50% por 3   segundos


sistema de camaras:

- la camara estara fija sobre el personaje principal.

- el angulo de vision del circuito sera desde arriba

- cuando jueguen dos jugadores la pantalla se dividira para mostrar   una pantalla para cada jugador

- la perspectiva del personaje sera en 3� persona.


Estructura del juego:

- se dividira en 3 torneos

- los dos primeros con 4 circuitos y la tercera con 5


Fisica del juego:

- 







 




